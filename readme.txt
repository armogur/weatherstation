docker run -it --rm -d -p 8080:80 --name web -v ~/site-content:/usr/share/nginx/html nginx
pio init --ide=clion
pio device monitor -b 115200
od -t u1 -v pressure-history

install cmake 3.21.*

ESP32 LittleFS based on: https://github.com/lorol/LITTLEFS
download https://github.com/earlephilhower/mklittlefs