import rcssmin
import os
import itertools
import esptool
import git
import time
import gzip
import subprocess

Import("env")


def minify_css():
    print("Minifying css file: src/html/static/style.css")
    with open("src/html/static/style.css", "r") as infile:
        return rcssmin.cssmin(infile.read())


def minify_javascript():
    print("Compiling JS frontend ...")
    result = subprocess.run(["bazel", "version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if result.stderr:
        raise esptool.FatalError("Error in acquiring bazel version: " + result.stderr)
    if result.returncode != 0:
        raise esptool.FatalError("Error in acquiring bazel version: " + str(result.returncode))
    if result.stdout.split("\n")[0] != "Build label: 4.2.1":
        raise esptool.FatalError("bazel version should be 4.2.1, current is: " + result.stdout.split("\n")[0])
    result = subprocess.run(["bazel", "build", "src/java/it/armogur/sensor/pollerapp"])
    if result.stderr:
        raise esptool.FatalError("Error in compiling JS frontend: " + result.stderr)
    if result.returncode != 0:
        raise esptool.FatalError("Error in compiling JS frontend: " + str(result.returncode))
    with open('bazel-bin/src/java/it/armogur/sensor/pollerapp/pollerapp.js', 'r') as file:
        return file.read()


def copy_index_html(dictionary):
    source_index_html = "src/html/index.html"
    target_index_html = "data/index.html"
    infile = open(source_index_html, "rt")
    outfile = open(target_index_html, "wt")
    for line in infile:
        for key, val in dictionary.items():
            line = line.strip().replace(key, val)
        outfile.write(line)
    infile.close()
    outfile.close()


def before_upload_little_fs(station_name, sha, date):
    print("Starting JS/CSS preprocessing")
    for r, d, f in itertools.chain(os.walk('data')):
        for file in f:
            print("Removing: " + os.path.join(r, file))
            os.remove(os.path.join(r, file))
    css = minify_css()
    js = minify_javascript()
    dictionary = {'SENSOR_POLLER_JS': js,
                  '<link rel="stylesheet" href="static/style.css">': '<style>' + css + '</style>',
                  'STATION_PLACE': station_name, 'REVISION': sha, 'COMMIT_DATE': date}
    copy_index_html(dictionary)
    with open('src/html/static/chart.min.js', 'rb') as uncompressed_chart_js:
        with gzip.open('data/static/chart.min.jgz', 'wb') as compressed_chart_js:
            compressed_chart_js.writelines(uncompressed_chart_js)
    print("Done")


def before_build():
    print("#################################################################")
    env.AutodetectUploadPort(env)
    upload_port = env.subst('$UPLOAD_PORT')
    chip = esptool.ESPLoader.detect_chip(port=upload_port)
    # print("CHIP ID: " + hex(chip.chip_id())) # ESP 32 not supports this

    mac_address = ''.join(map(lambda x: '%02x' % x, chip.read_mac())).upper()
    print("CHIP MAC: " + mac_address)

    os.makedirs("data/static", exist_ok=True)
    stations = {"84CCA8B272CF": {"name": "Kültér", "IP": "10.0.1.11", "P_ADJ": "1.0", "ALTITUDE": "110.0"},
                "84CCA8B08885": {"name": "Nagyszoba", "IP": "10.0.1.10", "P_ADJ": "0.0", "ALTITUDE": "109.0"},
                "78E36D0A2560": {"name": "Konyha", "IP": "10.0.1.12", "P_ADJ": "0.0", "ALTITUDE": "109.0"}}

    station = stations.get(mac_address)
    if station is None:
        raise esptool.FatalError("Can not find station for this MAC: " + mac_address)
    sn = station.get("name")
    s_ip = station.get("IP")
    p_adj = station.get("P_ADJ")
    altitude = station.get("ALTITUDE")
    print("Found station name: " + sn)
    print("Found station IP: " + s_ip)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    date = time.strftime("%Y.%m.%d. %H:%M:%S", time.localtime(repo.head.commit.committed_date))
    env.Append(CPPDEFINES=[('STATION_IP', s_ip), ('STATION_NAME', sn), ('ALTITUDE_IN_METERS', altitude),
                           ('PRESSURE_FINE_ADJUSTMENT', p_adj)])

    before_upload_little_fs(station_name=sn, sha=sha, date=date)
    # raise esptool.FatalError("##########################")


# env.AddPreAction("compiling", before_build)
before_build()

# raise esptool.FatalError("##########################")
