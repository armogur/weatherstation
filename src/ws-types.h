#ifndef WS_TYPES_H
#define WS_TYPES_H

#include <Arduino.h>
#include "Wire.h"

#ifdef ESP32
#define I2C_SDA 21
#define I2C_SCL 22
#elif defined(ESP8266)
#define I2C_SDA 4
#define I2C_SCL 5
#endif

#define BME280_I2C_ADDRESS      (0x76)

int8_t read8(byte reg, bool begin = false);
void write8(byte reg, byte value);
uint16_t read16(byte reg);
uint16_t read16_LE(byte reg);
int16_t readS16_LE(byte reg);
uint32_t read24(byte reg);

#endif