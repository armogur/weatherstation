#include "my-bme280.h"
#include "ws-types.h"

bme280_calib_data bme280Calibration;
int32_t t_fine;

BmeReadValues* readBme280() {
    int readCount = 0;
    uint16_t resetCount = 0;
    while (readCount < 5) {
        int32_t adc_T, adc_H, adc_P;

        if ((adc_T = read24(BME280_REGISTER_TEMPDATA)) == 0x800000
            || (adc_H = read16(BME280_REGISTER_HUMIDDATA))== 0x800000
            || (adc_P = read24(BME280_REGISTER_PRESSUREDATA)) == 0x800000) {
            i2cReset(false);
            resetCount++;
        } else {
            float temp = calculateTemperature(adc_T);
            auto humidity = (uint16_t)calculateHumidity(adc_H);
            float pressure = calculatePressure(adc_P);

            if (pressure < 900 * 100 || pressure > 1099 * 100 || temp < -99 * 100 || temp > 99 * 100) {
                i2cReset(false);
                resetCount++;
            } else {
                return new BmeReadValues{(int16_t)temp, humidity, (uint16_t)(pressure - 90000), resetCount};
            }
        }
        readCount++;
        delay(readCount * 20);
    }
    return nullptr;
}

float calculateTemperature(int32_t adc_T) {
    int32_t var1, var2;
    adc_T >>= 4;

    var1 = ((((adc_T >> 3) - ((int32_t)bme280Calibration.dig_T1 << 1))) *
            ((int32_t)bme280Calibration.dig_T2)) >> 11;

    var2 = (((((adc_T >> 4) - ((int32_t)bme280Calibration.dig_T1)) *
              ((adc_T >> 4) - ((int32_t)bme280Calibration.dig_T1))) >> 12) *
            ((int32_t)bme280Calibration.dig_T3)) >> 14;

    t_fine = var1 + var2;

    return (float)((t_fine * 5 + 128) >> 8);
}

float calculateHumidity(int32_t adc_H) {
    uint32_t v_x1_u32r;

    v_x1_u32r = (t_fine - ((int32_t)76800));

    v_x1_u32r = (((((adc_H << 14) - (((int32_t)bme280Calibration.dig_H4) << 20) -
                    (((int32_t)bme280Calibration.dig_H5) * v_x1_u32r)) +
                   ((int32_t)16384)) >> 15) *
                 (((((((v_x1_u32r * ((int32_t)bme280Calibration.dig_H6)) >> 10) *
                      (((v_x1_u32r * ((int32_t)bme280Calibration.dig_H3)) >> 11) +
                       ((int32_t)32768))) >> 10) +
                    ((int32_t)2097152)) *
                   ((int32_t)bme280Calibration.dig_H2) + 8192) >> 14));

    v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) *
                               ((int32_t)bme280Calibration.dig_H1)) >> 4));

    v_x1_u32r = (v_x1_u32r < 0) ? 0 : v_x1_u32r;
    v_x1_u32r = (v_x1_u32r > 419430400) ? 419430400 : v_x1_u32r;
    float h = 100.0f * (float)(v_x1_u32r >> 12);
    return h / 1024.0f;
}

float calculatePressure(int32_t adc_P) {
    int64_t var1, var2, p;
    adc_P >>= 4;

    var1 = ((int64_t)t_fine) - 128000;
    var2 = var1 * var1 * (int64_t)bme280Calibration.dig_P6;
    var2 = var2 + ((var1 * (int64_t)bme280Calibration.dig_P5) << 17);
    var2 = var2 + (((int64_t)bme280Calibration.dig_P4) << 35);
    var1 = ((var1 * var1 * (int64_t)bme280Calibration.dig_P3) >> 8) +
           ((var1 * (int64_t)bme280Calibration.dig_P2) << 12);
    var1 = (((((int64_t)1) << 47) + var1)) * ((int64_t)bme280Calibration.dig_P1) >> 33;

    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    p = 1048576 - adc_P;
    p = (((p << 31) - var2) * 3125) / var1;
    var1 = (((int64_t)bme280Calibration.dig_P9) * (p >> 13) * (p >> 13)) >> 25;
    var2 = (((int64_t)bme280Calibration.dig_P8) * p) >> 19;

    p = ((p + var1 + var2) >> 8) + (((int64_t)bme280Calibration.dig_P7) << 4);
    return ((float)p / 256 + (float )PRESSURE_FINE_ADJUST * 100.0f) / (float )pow(1.0 - (ALTITUDE / 44330.0), 5.255);
}

bool isReadingCalibration() {
    uint8_t const rStatus = read8(BME280_REGISTER_STATUS);
    return (rStatus & (1 << 0)) != 0;
}

void readCoefficients() {
    bme280Calibration.dig_T1 = read16_LE(BME280_REGISTER_DIG_T1);
    bme280Calibration.dig_T2 = readS16_LE(BME280_REGISTER_DIG_T2);
    bme280Calibration.dig_T3 = readS16_LE(BME280_REGISTER_DIG_T3);

    bme280Calibration.dig_H1 = read8(BME280_REGISTER_DIG_H1);
    bme280Calibration.dig_H2 = readS16_LE(BME280_REGISTER_DIG_H2);
    bme280Calibration.dig_H3 = read8(BME280_REGISTER_DIG_H3);
    bme280Calibration.dig_H4 = ((int8_t)read8(BME280_REGISTER_DIG_H4) << 4) |
                               (read8(BME280_REGISTER_DIG_H4 + 1) & 0xF);
    bme280Calibration.dig_H5 = ((int8_t)read8(BME280_REGISTER_DIG_H5 + 1) << 4) |
                               (read8(BME280_REGISTER_DIG_H5) >> 4);
    bme280Calibration.dig_H6 = (int8_t)read8(BME280_REGISTER_DIG_H6);

    bme280Calibration.dig_P1 = read16_LE(BME280_REGISTER_DIG_P1);
    bme280Calibration.dig_P2 = readS16_LE(BME280_REGISTER_DIG_P2);
    bme280Calibration.dig_P3 = readS16_LE(BME280_REGISTER_DIG_P3);
    bme280Calibration.dig_P4 = readS16_LE(BME280_REGISTER_DIG_P4);
    bme280Calibration.dig_P5 = readS16_LE(BME280_REGISTER_DIG_P5);
    bme280Calibration.dig_P6 = readS16_LE(BME280_REGISTER_DIG_P6);
    bme280Calibration.dig_P7 = readS16_LE(BME280_REGISTER_DIG_P7);
    bme280Calibration.dig_P8 = readS16_LE(BME280_REGISTER_DIG_P8);
    bme280Calibration.dig_P9 = readS16_LE(BME280_REGISTER_DIG_P9);
}

void setSampling(sensor_mode mode,sensor_sampling tempSampling,sensor_sampling pressSampling,
                 sensor_sampling humSampling,sensor_filter filter,standby_duration duration) {
    // making sure sensor is in sleep mode before setting configuration
    // as it otherwise may be ignored
    write8(BME280_REGISTER_CONTROL, sensor_mode::MODE_SLEEP);

    // you must make sure to also set REGISTER_CONTROL after setting the
    // CONTROLHUMID register, otherwise the values won't be applied (see
    // DS 5.4.3)
    write8(BME280_REGISTER_CONTROLHUMID, humSampling);
    unsigned int spi3w_en = 1;
    write8(BME280_REGISTER_CONFIG, (duration << 5) | (filter << 2) | spi3w_en);
    write8(BME280_REGISTER_CONTROL, (tempSampling << 5) | (pressSampling << 2) | mode);
}

int8_t i2cReset(bool booting) {
    pinMode(LED_BUILTIN, OUTPUT);

    pinMode(SDA, INPUT_PULLUP); // Make SDA (data) and SCL (clock) pins Inputs with pullup.
    pinMode(SCL, INPUT_PULLUP);

    if (booting) {
        delay(2500);  // Wait 2.5 secs. This is strictly only necessary on the first power
        // up of the DS3231 module to allow it to initialize properly,
        // but is also assists in reliable programming of FioV3 boards as it gives the
        // IDE a chance to start uploaded the program
        // before existing sketch confuses the IDE by sending Serial data.
    }

    boolean SCL_LOW = (digitalRead(SCL) == LOW); // Check is SCL is Low.
    if (SCL_LOW) { //If it is held low Arduino cannot become the I2C master.
        return 1; //I2C bus error. Could not clear SCL clock line held low
    }

    boolean SDA_LOW = (digitalRead(SDA) == LOW);  // vi. Check SDA input.
    int clockCount = 20; // > 2x9 clock

    while (SDA_LOW && (clockCount > 0)) { //  vii. If SDA is Low,
        clockCount--;
        // Note: I2C bus is open collector so do NOT drive SCL or SDA high.
        pinMode(SCL, INPUT); // release SCL pullup so that when made output it will be LOW
        pinMode(SCL, OUTPUT); // then clock SCL Low
        delayMicroseconds(10); //  for >5µS
        pinMode(SCL, INPUT); // release SCL LOW
        pinMode(SCL, INPUT_PULLUP); // turn on pullup resistors again
        // do not force high as slave may be holding it low for clock stretching.
        delayMicroseconds(10); //  for >5µS
        // The >5µS is so that even the slowest I2C devices are handled.
        SCL_LOW = (digitalRead(SCL) == LOW); // Check if SCL is Low.
        int counter = 20;
        while (SCL_LOW && (counter > 0)) {  //  loop waiting for SCL to become High only wait 2sec.
            counter--;
            delay(100);
            SCL_LOW = (digitalRead(SCL) == LOW);
        }
        if (SCL_LOW) { // still low after 2 sec error
            return 2; // I2C bus error. Could not clear. SCL clock line held low by slave clock stretch for >2sec
        }
        SDA_LOW = (digitalRead(SDA) == LOW); //   and check SDA input again and loop
    }
    if (SDA_LOW) { // still low
        return 3; // I2C bus error. Could not clear. SDA data line held low
    }

    // else pull SDA line low for Start or Repeated Start
    pinMode(SDA, INPUT); // remove pullup.
    pinMode(SDA, OUTPUT);  // and then make it LOW i.e. send an I2C Start or Repeated start control.
    // When there is only one I2C master a Start or Repeat Start has the same function as a Stop and clears the bus.
    /// A Repeat Start is a Start occurring after a Start with no intervening Stop.
    delayMicroseconds(10); // wait >5µS
    pinMode(SDA, INPUT); // remove output low
    pinMode(SDA, INPUT_PULLUP); // and make SDA high i.e. send I2C STOP control.
    delayMicroseconds(10); // x. wait >5µS
    pinMode(SDA, INPUT); // and reset pins as tri-state inputs which is the default state on reset
    pinMode(SCL, INPUT);

    // Power ON the BME280 board
    pinMode(BME_PWR, OUTPUT);
    digitalWrite(BME_PWR, HIGH);

    int8_t _sensorID;
    int counter = 0;
    do {
        counter++;
        _sensorID = read8(BME280_REGISTER_CHIPID, true);
        if (_sensorID != 0x60) {
            digitalWrite(BME_PWR, LOW);
            delay(50);
            digitalWrite(BME_PWR, HIGH);
        }
        Serial.printf("Trying BME: %u\n", counter);
    } while (_sensorID != 0x60);

    // reset the device using soft-reset
    // this makes sure the IIR is off, etc.
    write8(BME280_REGISTER_SOFTRESET, 0xB6);

    // wait for chip to wake up.
    delay(10);

    // if chip is still reading calibration, delay
    while (isReadingCalibration())
        delay(10);

    if (booting) {
        readCoefficients(); // read trimming parameters, see DS 4.2.2
    }

    setSampling(); // use defaults
    delay(100);
    pinMode(LED_BUILTIN, INPUT);

    return 0; // all ok
}