#ifndef MY_GPS_H
#define MY_GPS_H

#include <cstdint>
#include <cstring>
#include <cstdlib>
#include <cctype>
#include <cstdio>
#include <HardwareSerial.h>

#define MY_GPS_GP_RMC           "GPRMC"
#define MY_GPS_GN_RMC           "GNRMC"

#define MY_GPS_GP_GGA           "GPGGA"
#define MY_GPS_GN_GGA           "GNRMC"

#define MY_GPS_MAX_FIELD_SIZE   15
#define MY_GPS_MILES_PER_METER  0.00062137112
#define MY_GPS_KM_PER_METER     0.001
#define MY_GPS_FEET_PER_METER   3.2808399

#define COMBINE(sentence_type, term_number) (((unsigned)(sentence_type) << 5) | term_number)

struct MyGPSTime {
    friend class MyGPS;
public:
    uint8_t getHour() { return hour; }
    uint8_t getMinute() { return minute; }
    uint8_t getSecond() { return second; }
    uint8_t getCentiSecond() { return centiSecond; }

private:
    void setTime(const char *term);
    void reset();
    uint8_t hour, minute, second, centiSecond;
};

struct MyGPSDate {
    friend class MyGPS;

public:
    uint16_t getYear();
    uint8_t getMonth();
    uint8_t getDay();

private:
    void setDate(const char *term);
    void reset();
    uint16_t year;
    uint8_t month, day;
};

struct Coordinate {
public:
    Coordinate() {
        this->reset();
    }
    Coordinate(uint8_t deg, uint8_t minutes, float seconds) {
        this->deg = deg;
        this->minutes = minutes;
        this->seconds = seconds;
    }

    void reset() {
        deg = 222;
        minutes = 222;
        seconds = 222;
    }

    uint8_t getDeg() { return deg; }
    uint8_t getMinutes() { return minutes; }
    float getSeconds() { return seconds; }
    char getOrientation() { return orientation; }

    void setOrientation(char orientation) { this->orientation = orientation; }
private:
    uint8_t deg, minutes;
    float seconds;
    char orientation;
};

struct MyGPSLocation {
    friend class MyGPS;

public:
    void parseDegrees(const char *term);

    Coordinate &getLatitude() { return latitude; }
    Coordinate &getLongitude() { return longitude; }

private:
    uint8_t tempDeg, tempMinutes;
    float tempSeconds;
    Coordinate latitude{};
    Coordinate longitude{};
    void setLatitude(const char *term);
    void setLatitudeOrientation(char orientation) { latitude.setOrientation(orientation); }
    void setLongitude(const char *term);
    void setLongitudeOrientation(char orientation) { longitude.setOrientation(orientation); }
    void reset();
};

struct MyGPSAltitude {
    friend class MyGPS;

public:
    double meters() { return val / 100.0; }
    double miles() { return MY_GPS_MILES_PER_METER * val / 100.0; }
    double kilometers() { return MY_GPS_KM_PER_METER * val / 100.0; }
    double feet() { return MY_GPS_FEET_PER_METER * val / 100.0; }

private:
    int32_t val;
    void setAltitude(const char *term);
    void reset() { val = -777; }
};

class MyGPS {
public:
    MyGPS(HardwareSerial &gpsSerial);
    static int32_t parseDecimal(const char *term);
    enum MessageType {VTG, GSV, GSA, GLL, GGA, RMC};
    enum SentenceType {GPS_SENTENCE_GP_GGA, GPS_SENTENCE_GP_RMC, GPS_SENTENCE_OTHER};
    bool encode(char c);
    bool isValid() { return valid; }
    bool isChecksumValid() { return checksumValid; }

    int nemaMsgDisable(MessageType msgType);
    int nemaMsgEnable(MessageType msgType);

    MyGPSLocation &getLocation() { return location; }
    MyGPSAltitude &getAltitude() { return altitude; }
    MyGPSTime getTime() { return time; }
    SentenceType getSentenceType() { return curSentenceType; }

private:
    const char *messageTypes[6] = {
            __STRINGIFY(VTG), __STRINGIFY(GSV), __STRINGIFY(GSA),
            __STRINGIFY(GLL), __STRINGIFY(GGA), __STRINGIFY(RMC)
    };
    void endOfTermHandler();

    char term[MY_GPS_MAX_FIELD_SIZE]{};
    uint8_t parity = 0;
    bool isChecksumTerm = false;
    uint8_t curTermOffset = 0;
    uint8_t curTermNumber = 0;
    SentenceType curSentenceType = GPS_SENTENCE_OTHER;
    HardwareSerial& gpsSerial;

    MyGPSTime time{};
    MyGPSDate date{};
    MyGPSLocation location{};
    MyGPSAltitude altitude{};
    bool valid = false;
    bool checksumValid = false;
    bool toReset = true;
    inline int calculateChecksum (const char *msg);
    inline int nemaMsgSend(const char *msg);
    void reset();

    inline int fromHex(char a);
};

#endif