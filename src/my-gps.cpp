#include "my-gps.h"

void MyGPSTime::setTime(const char *term) {
    auto time = MyGPS::parseDecimal(term);
    hour = time / 1000000;
    minute = (time / 10000) % 100;
    second = (time / 100) % 100;
    centiSecond = time % 100;
}

void MyGPSTime::reset() {
    hour = 111;
    minute = 111;
    second = 111;
    centiSecond = 111;
}

void MyGPSDate::setDate(const char *term) {
    auto newDate = atol(term);
    year = (newDate % 100) + 2000;
    month = (newDate / 100) % 100;
    day = newDate / 10000;
}

void MyGPSDate::reset() {
    year = 111;
    month = 111;
    day = 111;
}

void MyGPSLocation::setLatitude(const char *term) {
    parseDegrees(term);
    latitude = Coordinate(tempDeg, tempMinutes, tempSeconds);
}

void MyGPSLocation::setLongitude(const char *term) {
    parseDegrees(term);
    longitude = Coordinate(tempDeg, tempMinutes, tempSeconds);
}

void MyGPSLocation::reset() {
    latitude.reset();
    longitude.reset();
}

void MyGPSAltitude::setAltitude(const char *term) {
    val = MyGPS::parseDecimal(term);
}

MyGPS::MyGPS(HardwareSerial &gpsSerial) : gpsSerial(gpsSerial) {
    term[0] = '\0';
}

bool MyGPS::encode(char c) {
    if (toReset) {
        reset();
    }
    switch(c) {
        case ',':
            parity ^= (uint8_t)c;
        case '\r':
        case '\n':
        case '*': {
            if (curTermOffset < sizeof(term)) {
                term[curTermOffset] = 0;
                endOfTermHandler();
            }
            ++curTermNumber;
            curTermOffset = 0;
            isChecksumTerm = c == '*';
            return false;
        }

        case '$': // sentence begin
            toReset = true;
            return valid;
        default: // ordinary characters
            if (curTermOffset < sizeof(term) - 1) {
                term[curTermOffset++] = c;
            }
            if (!isChecksumTerm) {
                parity ^= c;
            }
            return false;
    }
}

void MyGPS::endOfTermHandler() {
    if (isChecksumTerm) {
        uint8_t checksum = 16 * fromHex(term[0]) + fromHex(term[1]);
        checksumValid = checksum == parity;
        return;
    }
    // the first term determines the sentence type
    if (curTermNumber == 0) {
        if (!strcmp(term, MY_GPS_GP_RMC) || !strcmp(term, MY_GPS_GN_RMC)) {
            curSentenceType = GPS_SENTENCE_GP_RMC;
        } else if (!strcmp(term, MY_GPS_GP_GGA) || !strcmp(term, MY_GPS_GN_GGA)) {
            curSentenceType = GPS_SENTENCE_GP_GGA;
        } else {
            curSentenceType = GPS_SENTENCE_OTHER;
        }
        return;
    }

    if (curSentenceType != GPS_SENTENCE_OTHER && term[0])
        switch (COMBINE(curSentenceType, curTermNumber)) {
            case COMBINE(GPS_SENTENCE_GP_RMC, 1): // Time in both sentences
            case COMBINE(GPS_SENTENCE_GP_GGA, 1):
                time.setTime(term);
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 2): // GPRMC validity
                valid = term[0] == 'A';
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 3): // Latitude
            case COMBINE(GPS_SENTENCE_GP_GGA, 2):
                location.setLatitude(term);
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 4): // N/S
            case COMBINE(GPS_SENTENCE_GP_GGA, 3):
                location.setLatitudeOrientation(term[0]);
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 5): // Longitude
            case COMBINE(GPS_SENTENCE_GP_GGA, 4):
                location.setLongitude(term);
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 6): // E/W
            case COMBINE(GPS_SENTENCE_GP_GGA, 5):
                location.setLongitudeOrientation(term[0]);
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 7): // Speed (GPRMC)
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 8): // Course (GPRMC)
                break;
            case COMBINE(GPS_SENTENCE_GP_RMC, 9): // Date (GPRMC)
                date.setDate(term);
                break;
            case COMBINE(GPS_SENTENCE_GP_GGA, 6): // Position Fix Indicator
                valid = term[0] > '0';
                break;
            case COMBINE(GPS_SENTENCE_GP_GGA, 9): // MSL Altitude
                altitude.setAltitude(term);
                break;
        }
}

int32_t MyGPS::parseDecimal(const char *term) {
    bool negative = *term == '-';
    if (negative) ++term;
    int32_t ret = 100 * (int32_t)atol(term);
    while (isdigit(*term)) ++term;
    if (*term == '.' && isdigit(term[1])) {
        ret += 10 * (term[1] - '0');
        if (isdigit(term[2])) {
            ret += term[2] - '0';
        }
    }
    return negative ? -ret : ret;
}

void MyGPSLocation::parseDegrees(const char *term) {
    auto leftOfDecimal = (uint32_t)atol(term);
    tempMinutes = (uint16_t)(leftOfDecimal % 100);
    tempDeg = (uint16_t)(leftOfDecimal / 100);

    while (isdigit(*term)) {
        ++term;
    }

    if (*term == '.') {
        ++term;
    }
    uint32_t rightOfDecimal = atol(term);
    tempSeconds = (float )rightOfDecimal;
    while (tempSeconds > 1) {
        tempSeconds /= 10.0;
    }
    tempSeconds = 60.0f * tempSeconds;
}

inline int MyGPS::calculateChecksum(const char *msg) {
    int checksum = 0;
    for (int i = 0; msg[i] && i < 32; i++) {
        checksum ^= (unsigned char) msg[i];
    }

    return checksum;
}

inline int MyGPS::nemaMsgSend(const char *msg) {
    char checksum[8];
    snprintf(checksum, sizeof(checksum) - 1, "*%.2X", calculateChecksum(msg));
    gpsSerial.print("$");
    gpsSerial.print(msg);
    gpsSerial.println(checksum);
    return 0;
}

int MyGPS::nemaMsgDisable(MessageType msgType) {
    char tmp[32];
    snprintf(tmp, sizeof(tmp)-1, "PUBX,40,%s,0,0,0,0", messageTypes[msgType]);
    nemaMsgSend(tmp);

    return 1;
}

int MyGPS::nemaMsgEnable(MessageType msgType) {
    char tmp[32];
    snprintf(tmp, sizeof(tmp) - 1, "PUBX,40,%s,0,1,0,0", messageTypes[msgType]);
    nemaMsgSend(tmp);

    return 1;
}

void MyGPS::reset() {
    time.reset();
    date.reset();
    location.reset();
    altitude.reset();
    curTermNumber = curTermOffset = 0;
    curSentenceType = GPS_SENTENCE_OTHER;
    valid = false;
    checksumValid = false;
    parity = 0;
    isChecksumTerm = false;
    toReset = false;
}

inline int MyGPS::fromHex(char a) {
    if (a >= 'A' && a <= 'F')
        return a - 'A' + 10;
    else if (a >= 'a' && a <= 'f')
        return a - 'a' + 10;
    else
        return a - '0';
}