#include <Arduino.h>
#include <Wire.h>
#ifdef ESP32
#include "esp32-little-fs/LittleFs.h"
#include <WiFi.h>
#include <AsyncTCP.h>
#elif defined(ESP8266)
#include <LittleFS.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ctime>
#include "wifi-config.h"
#include "my-bme280.h"
#include "main.h"
//#include "my-gps.h"
#include <exception>

#define STR(x)                  __STRINGIFY(x)
#define max_history_len         (4 * 24 * 6)

timeval tv;
uint32_t currentTimeStamp, startTime = 0, lastTimeStamp, lastSensorReadTime;
char lastModified[30], staticLastModified[30];

AsyncWebServer server(80);

PressureItem pressureHistory[max_history_len];
uint16_t historySize;
DataHolder dataHolderSnapshot =  {-1, 0, 0, 0, 0, 0};
//uint32_t gpsLastRead;
//HardwareSerial gpsSerial = Serial2;
//MyGPS gps(gpsSerial);

void setup() {
    initSerials();

    sprintf(staticLastModified, "%s %s GMT", __DATE__, __TIME__);
    sprintf(lastModified, "%s %s GMT", __DATE__, __TIME__);

    try {
        initWifi();
    } catch (const std::exception& e) {
        Serial.println(e.what());
        return;
    }

    initI2C();
    configTime(3600, 3600, "time.kfki.hu");

    if (!LittleFS.begin()) {
        Serial.println(F("An Error has occurred while mounting filesystem"));
        return;
    }
    readSensor(millis() / 1000);
    dataHolderSnapshot.freeHeap = ESP.getFreeHeap();
    initWebServer();
}

void initSerials() {
    Serial.begin(115200);
    while (!Serial) {
        yield();
    }
    Serial.println();
//    gpsSerial.begin(9600);
//    while (!gpsSerial) {
//        yield();
//    }
//    gps.nemaMsgDisable(MyGPS::MessageType::VTG);
//    gps.nemaMsgDisable(MyGPS::MessageType::GSV);
//    gps.nemaMsgDisable(MyGPS::MessageType::GSA);
//    gps.nemaMsgDisable(MyGPS::MessageType::GLL);
//    gps.nemaMsgDisable(MyGPS::MessageType::RMC);
//
//    gps.nemaMsgEnable(MyGPS::MessageType::GGA);
}

void initWifi() {
    const char* stationIp = STR(STATION_IP);

    int a, b, c, d;
    if (sscanf(stationIp, "%d.%d.%d.%d", &a, &b, &c, &d) == 4) {
        WiFi.config(IPAddress(a, b, c, d),
                    IPAddress(10,0,1,1),
                    IPAddress(255,255,255,0),
                    IPAddress(8,8,8,8),
                    IPAddress(8,8,4,4));
        WiFi.mode(WIFI_STA);
        WiFi.begin(CFG_WIFI_SSID, CFG_WIFI_PASS);
        //check wi-fi is connected to wi-fi network
        char counter = 0;
        while (!WiFi.isConnected()) {
            delay(1000);
            Serial.print(".");
            counter++;
            if (counter > 10) {
                ESP.restart();
            }
        }
        Serial.println();
        Serial.println(F("WiFi connected!"));
    } else {
        char result[300];
        sprintf(result, "Wrong IP: %s", stationIp);
        throw std::runtime_error(result);
    }
}

void initI2C() {
    int8_t cleared = i2cReset(true); // clear the I2C bus first before calling Wire.begin()
    if (cleared != 0) {
        Serial.println("I2C bus error. Could not clear");
        if (cleared == 1) {
            Serial.println("SCL clock line held low");
        } else if (cleared == 2) {
            Serial.println("SCL clock line held low by slave clock stretch");
        } else if (cleared == 3) {
            Serial.println("SDA data line held low");
        }
    } else {
        Serial.println("I2C cleared");
    }
}

void loop() {
    // in seconds!
    // https://github.com/esp8266/Arduino/blob/61cd8d83859524db0066a647de3de3f6a0039bb2/libraries/esp8266/examples/NTP-TZ-DST/NTP-TZ-DST.ino
    gettimeofday(&tv, nullptr);

    currentTimeStamp = (uint32_t)tv.tv_sec;
    uint32_t uptime = millis() / 1000;
    if (uptime != lastSensorReadTime && uptime % 10 == 0) {
        readSensor(uptime);
    }
//    if (uptime - gpsLastRead > 1) {
//        gpsLastRead = uptime;
//        while (gpsSerial.available() > 0) {
//            char gpsData = (char )gpsSerial.read();
//            gps.encode(gpsData);
//            if (gpsData == '$' && gps.isValid()) {
//                char printHelper[300];
//                sprintf(printHelper, "T: %02d:%02d:%02d, loc = ", gps.getTime().getHour(), gps.getTime().getMinute(), gps.getTime().getSecond());
//                Serial.print(printHelper);
//
//                Coordinate latitude = gps.getLocation().getLatitude();
//                sprintf(printHelper, "%hhu°%hhu'%2.1f\"%c", latitude.getDeg(), latitude.getMinutes(), latitude.getSeconds(), latitude.getOrientation());
//                Serial.print(printHelper);
//                Serial.print(", ");
//
//                Coordinate longitude = gps.getLocation().getLongitude();
//                sprintf(printHelper, "%hhu°%hhu'%2.1f\"%c", longitude.getDeg(), longitude.getMinutes(), longitude.getSeconds(), longitude.getOrientation());
//                Serial.print(printHelper);
//                Serial.print(", altitude = ");
//                Serial.print(gps.getAltitude().meters());
//                Serial.print(" m,  ");
//                Serial.print(gps.getAltitude().feet());
//                Serial.print(" feet, checksum: ");
//                Serial.println(gps.isChecksumValid() ? "✔" : "✘");
//            }
//            Serial.print(gpsData);
//        }
//    }
    if (startTime > 0) {
        dataHolderSnapshot.uptime = currentTimeStamp - startTime;
    }
    if (currentTimeStamp - lastTimeStamp > 600) {
        if (startTime == 0 && currentTimeStamp > uptime) {
            startTime = currentTimeStamp - uptime;
        }
        BmeReadValues* readValues = readBme280();
        if (readValues != nullptr) {
            lastTimeStamp = currentTimeStamp;
            auto raw_time = (time_t) lastTimeStamp;
            struct tm *ptm = localtime (&raw_time);
            strftime(lastModified, 30, "%a, %d %b %Y %H:%M:%S %Z", ptm);
            if (historySize < max_history_len) {
                pressureHistory[historySize] = { currentTimeStamp,
                                                 readValues->temperature, readValues->humidity, readValues->pressure };
                historySize++;
            } else {
                memmove(pressureHistory, &pressureHistory[1], sizeof(pressureHistory) - sizeof(pressureHistory[0]));
                pressureHistory[max_history_len - 1].time = currentTimeStamp;
                pressureHistory[max_history_len - 1].temperature = readValues->temperature;
                pressureHistory[max_history_len - 1].humidity = readValues->humidity;
                pressureHistory[max_history_len - 1].pressure = readValues->pressure;
            }
            if (readValues->resetCount > 0) {
                dataHolderSnapshot.lastI2CReset = currentTimeStamp;
            }
            free(readValues);
        }
    }
}

void readSensor(uint32_t uptime) {
    BmeReadValues* readValues = readBme280();
    lastSensorReadTime = uptime;
    dataHolderSnapshot.temperature = readValues->temperature;
    dataHolderSnapshot.humidity = readValues->humidity;
    dataHolderSnapshot.pressure = readValues->pressure;
    if (readValues->resetCount > 0) {
        dataHolderSnapshot.lastI2CReset = currentTimeStamp;
    }
    free(readValues);
}

void pressureBinaryHistory(AsyncWebServerRequest *request) {
    AsyncWebHeader* ifModifiedSince = request->getHeader(F("If-Modified-Since"));
    if (ifModifiedSince != nullptr && ifModifiedSince->value().equals(lastModified)) {
        request->send(304);
    } else {
        AsyncWebServerResponse * response = request->beginResponse_P(200, "application/octet-stream",
                                                                     (uint8_t *) &pressureHistory,
                                                                     historySize * sizeof(pressureHistory[0]));
        response->addHeader(F("Last-Modified"), lastModified);
        request->send(response);
        dataHolderSnapshot.freeHeap = ESP.getFreeHeap();
    }
}

void serveChartJs(AsyncWebServerRequest *request) {
    AsyncWebHeader* ifModifiedSince = request->getHeader(F("If-Modified-Since"));
    if (ifModifiedSince != nullptr && ifModifiedSince->value().equals(staticLastModified)) {
        request->send(304);
    } else {
        AsyncWebServerResponse * response = request->beginResponse(LittleFS, "/static/chart.min.jgz", "application/javascript");
        response->addHeader(F("Content-Encoding"), "gzip");
        response->addHeader(F("Last-Modified"), staticLastModified);
        request->send(response);
    }
}

void initWebServer() {
    server.rewrite("/", "/index.html");
    server.serveStatic("/index.html", LittleFS, "/index.html").setLastModified(staticLastModified);
    server.on("/heap", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/plain", String(ESP.getFreeHeap()));
    });
    server.on("/data", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "application/octet-stream", (uint8_t *) &dataHolderSnapshot, sizeof(dataHolderSnapshot));
    });
    server.on("/pressure-history", HTTP_GET, pressureBinaryHistory);
    server.on("/static/chart.min.js", HTTP_GET, serveChartJs);

    server.onNotFound([](AsyncWebServerRequest *request) {
        request->send(404, F("text/plain"), F("404: Not Found"));
    });
    server.begin();
    Serial.println(F("HTTP server started"));
}