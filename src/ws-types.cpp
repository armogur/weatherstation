#include "ws-types.h"

int8_t read8(byte reg, bool begin) {
    int8_t value;

    if (begin) {
        Wire.begin(I2C_SDA, I2C_SCL);
    }
    Wire.beginTransmission((uint8_t)BME280_I2C_ADDRESS);
    Wire.write((uint8_t)reg);
    Wire.endTransmission();
    Wire.requestFrom((uint8_t)BME280_I2C_ADDRESS, (byte)1);

    value = (int8_t)Wire.read();

    return value;
}

uint16_t read16(byte reg) {
    uint16_t value;

    Wire.beginTransmission((uint8_t)BME280_I2C_ADDRESS);
    Wire.write((uint8_t)reg);
    Wire.endTransmission();
    Wire.requestFrom((uint8_t)BME280_I2C_ADDRESS, (byte)2);
    value = (Wire.read() << 8) | Wire.read();

    return value;
}

uint32_t read24(byte reg) {
    uint32_t value;

    Wire.beginTransmission((uint8_t)BME280_I2C_ADDRESS);
    Wire.write((uint8_t) reg);
    Wire.endTransmission();
    Wire.requestFrom((uint8_t)BME280_I2C_ADDRESS, (byte)3);

    value = Wire.read();
    value <<= 8;
    value |= Wire.read();
    value <<= 8;
    value |= Wire.read();

    return value;
}

uint16_t read16_LE(byte reg) {
    uint16_t temp = read16(reg);
    return (temp >> 8) | (temp << 8);
}

int16_t readS16_LE(byte reg) {
    return (int16_t)read16_LE(reg);
}

void write8(byte reg, byte value) {
    Wire.beginTransmission((uint8_t)BME280_I2C_ADDRESS);
    Wire.write((uint8_t)reg);
    Wire.write((uint8_t)value);
    Wire.endTransmission();
}