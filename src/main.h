typedef struct {
    uint32_t time;
    int16_t temperature;
    uint16_t humidity;
    uint16_t pressure;
} PressureItem;

typedef struct {
    int16_t temperature;
    uint16_t humidity, pressure;
    uint32_t uptime, lastI2CReset, freeHeap;
} DataHolder;

void initSerials();
void initWebServer();
void initWifi();
void initI2C();

void pressureBinaryHistory(AsyncWebServerRequest *request);
void serveChartJs(AsyncWebServerRequest *request);
void readSensor(uint32_t uptime);