package it.armogur.sensor.pollerlib;

import jsinterop.annotations.*;
import elemental2.core.*;
import elemental2.dom.*;
import jsinterop.base.*;

public class SensorPoller {
	private static final int ONE_MINUTE = 60;
	private static final int ONE_HOUR = 60 * ONE_MINUTE;
	private static final int ONE_DAY = 24 * ONE_HOUR;
	private static final JsObject SENSOR_HISTORY_TIME_FORMAT;
	private static final JsObject I2C_RESET_DATE_FORMAT;

	static {
		JsMap<Object, Object> formatMap = new JsMap<>();
		formatMap.set("weekday", "long");
		formatMap.set("hour", "2-digit");
		formatMap.set("minute", "2-digit");

		JsIterable<Object> formatEntries = Js.cast(formatMap.entries());
		SENSOR_HISTORY_TIME_FORMAT = JsObject.fromEntries(formatEntries);

		formatMap = new JsMap<>();
		formatMap.set("year", "numeric");
		formatMap.set("month", "long");
		formatMap.set("day", "numeric");
		formatMap.set("weekday", "long");
		formatMap.set("hour", "2-digit");
		formatMap.set("minute", "2-digit");
		formatMap.set("second", "2-digit");

		formatEntries = Js.cast(formatMap.entries());
		I2C_RESET_DATE_FORMAT = JsObject.fromEntries(formatEntries);
	}

	private static final XMLHttpRequest.OnerrorFn DATA_ON_ERROR = e -> {
		setInnerHtml("ig", "Adat lekérdezés sikertelen!");
		return null;
	};

	private static final XMLHttpRequest.OnerrorFn SENSOR_HISTORY_ON_ERROR = e -> {
		setInnerHtml("ig", "Érzékelő történet lekérdezés sikertelen!");
		return null;
	};

	private static final DomGlobal.SetIntervalCallbackFn DATA_GETTER_CALLBACK = createCallback("/data", DATA_ON_ERROR,
			SensorPoller::presentData);

	private static final DomGlobal.SetIntervalCallbackFn SENSOR_HISTORY_GETTER_CALLBACK = createCallback("/pressure-history",
			SENSOR_HISTORY_ON_ERROR, SensorPoller::presentSensorHistory);

	private interface DataPresenter {
		void apply(Uint8Array b);
	}

	private static DomGlobal.SetIntervalCallbackFn createCallback(final String url, final XMLHttpRequest.OnerrorFn onerror,
			final DataPresenter dataPresenter) {
		return a -> {
			XMLHttpRequest xmlHttpRequest = new XMLHttpRequest();
			xmlHttpRequest.open("GET", url, true);
			xmlHttpRequest.responseType = "arraybuffer";
			xmlHttpRequest.timeout = 5000;
			xmlHttpRequest.onerror = onerror;
			xmlHttpRequest.onload = p0 -> {
				if (xmlHttpRequest.status != 200) {
					String s;
					if (xmlHttpRequest.status == 404) {
						s = "Hiba " + xmlHttpRequest.responseURL + " nem található!";
					} else {
						s = "Hiba " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText;
					}
					setInnerHtml("ig", s);
				} else {
					JsObject jsObject = xmlHttpRequest.response.asJsObject();
					ArrayBuffer arrayBuffer = Js.cast(jsObject);
					dataPresenter.apply(new Uint8Array(arrayBuffer));
				}
			};
			xmlHttpRequest.send();
		};
	}

	private static String twoDigits(int n) {
		return n < 10 ? "0" + n : "" + n;
	}

	private static String uptime(int u) {
		int weeks = u / ONE_DAY / 7;
		int days = (u % (ONE_DAY * 7)) / ONE_DAY;
		String result;
		if (weeks > 0) {
			result = weeks + " hét " + days + " nap ";
		} else if (days > 0) {
			result = days + " nap ";
		} else {
			result = "";
		}
		return result + twoDigits((u % ONE_DAY) / ONE_HOUR) + ":"  // hour
				+ twoDigits((u % ONE_HOUR) / ONE_MINUTE) + ":"  // minute
				+ twoDigits(u % ONE_MINUTE);  // seconds
	}

	private static int binaryToInt(Uint8Array binaryArray, int from, int length, boolean signed) {
		int result = 0;
		for (int i = from; i < from + length; i++) {
			result += Math.pow(256, (i - from)) * binaryArray.getAtAsAny(i).asInt();
		}
		if (signed && binaryArray.getAtAsAny(from + length - 1).asInt() >= 128) {
			result -= Math.pow(256, length);
		}
		return result;
	}

	private static void setInnerHtml(String id, String value) {
		DomGlobal.document.getElementById(id).innerHTML = value;
	}

	private static void presentData(Uint8Array b) {
		setInnerHtml("ia", (Math.round(binaryToInt(b, 0, 2, true) / 10.0) / 10.0) + " °C");
		setInnerHtml("ib", (Math.round(binaryToInt(b, 2, 2, false) / 10.0) / 10.0) + " %");
		setInnerHtml("ic", (Math.round((binaryToInt(b, 4, 2, false) + 90_000) / 10.0) / 10.0) + " ㍱");
		// C++ puts 2 bytes hole here
		setInnerHtml("if", uptime(binaryToInt(b, 8, 4, false)));
		int l = binaryToInt(b, 12, 4, false);

		if (l > 0) {
			JsDate jsDate = new JsDate();
			jsDate.setTime(l * 1_000);
			setInnerHtml("ig", "Érzékelő újraindítása: " + jsDate.toLocaleTimeString("HU", I2C_RESET_DATE_FORMAT) + " volt.");
		} else {
			setInnerHtml("ig", "Érzékelőt még nem kellett újraindítani!");
		}
		setInnerHtml("ie", binaryToInt(b, 16, 4, false) + " byte");
	}

	private static void presentSensorHistory(Uint8Array payload) {
		final int size = payload.length / 12;
		String[] timeLabels = new String[size];
		double[] temperatures = new double[size];
		double[] relativeHumidities = new double[size];
		double[] airPressures = new double[size];

		int j = 0;
		int index = 0;
		while (j < payload.length) {
			JsDate jsDate = new JsDate();
			jsDate.setTime(binaryToInt(payload, j, 4, false) * 1_000L);
			timeLabels[index] = jsDate.toLocaleTimeString("HU", SENSOR_HISTORY_TIME_FORMAT);
			j += 4;

			temperatures[index] = Math.round(binaryToInt(payload, j, 2, true) / 10.0) / 10.0;
			j += 2;
			relativeHumidities[index] = Math.round(binaryToInt(payload, j, 2, false) / 10.0) / 10.0;
			j += 2;
			airPressures[index] = Math.round((binaryToInt(payload, j, 2, false) + 90_000.0) / 10.0) / 10.0;
			j += 4; // C++ puts 2 bytes hole here
			index++;
		}
		updateChart(timeLabels, temperatures, relativeHumidities, airPressures);
	}

	@JsMethod(namespace = "j2cl.sensor.pollerjs")
	public static native void updateChart(String[] timeLabels, double[] temperatures, double[] relativeHumidities, double[] airPressures);

	@JsMethod
	public static void init() {
		DATA_GETTER_CALLBACK.onInvoke((Object) null);
		SENSOR_HISTORY_GETTER_CALLBACK.onInvoke((Object) null);
		DomGlobal.setInterval(DATA_GETTER_CALLBACK, 10_000);
		DomGlobal.setInterval(SENSOR_HISTORY_GETTER_CALLBACK, 60_000);
	}
}
