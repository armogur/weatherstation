goog.module('j2cl.sensor.pollerjs');

const SensorPoller = goog.require('it.armogur.sensor.pollerlib.SensorPoller');

/**
 * @suppress {undefinedVars}
 */
function init() {
    chart = new Chart("ih", {
        "type": "line",
        "data": {
            "datasets": [
                {
                    "backgroundColor": "#FF640059",
                    "label": "Hőmérséklet",
                    "pointRadius": 0,
                    "borderColor": [
                        "#FF0000FF",
                        "#36A2EBFF",
                        "#FFCE56FF",
                        "#4BC0C0FF",
                        "#9966FFFF",
                        "#FF9F40FF"
                    ],
                    "borderWidth": 2,
                    "yAxisID": 'yT'
                },
                {
                    "backgroundColor": "#00FF9699",
                    "label": "Relatív páratartalom",
                    "pointRadius": 0,
                    "borderColor": [
                        "#00FF00FF",
                        "#00FF00FF",
                        "#00FF00FF",
                        "#00FF00FF",
                        "#00FF00FF",
                        "#00FF00FF",
                    ],
                    "borderWidth": 2,
                    "yAxisID": 'yR'
                },
                {
                    "backgroundColor": "#0032EB99",
                    "fill": "start",
                    "label": "Légnyomás",
                    "pointRadius": 0,
                    "borderColor": [
                        "#FF6384FF",
                        "#36A2EBFF",
                        "#FFCE56FF",
                        "#4BC0C0FF",
                        "#9966FFFF",
                        "#FF9F40FF"
                    ],
                    "borderWidth": 1,
                    "yAxisID": 'yP',
                }]
        },
        "options": {
            "responsive": true,
            "elements": {
                "line": {
                    "tension": 0.4
                }
            },
            "legend": {
                "display": true
            },
            "interaction": {
                "mode": 'index',
                "intersect": false,
            },
            "hover": {
                "mode": 'nearest',
                "intersect": true
            },
            "scales": {
                "xAxis": {
                    "display": true,
                    "title": {
                        "display": true,
                        "text": 'mérés időpontja'
                    }
                },
                "yP": {
                    "type": 'linear',
                    "display": true,
                    "position": 'left',
                    "title": {
                        "display": true,
                        "text": 'légnyomás'
                    },
                    "ticks": {
                        "callback": function(v) {
                            return v.toFixed(1) + ' ㍱';
                        }
                    }
                }, "yT": {
                    "type": 'linear',
                    "display": true,
                    "position": 'right',
                    "title": {
                        "display": true,
                        "text": 'hőmérséklet'
                    },
                    "ticks": {
                        "callback": function(v) {
                            return v.toFixed(1) + ' °C';
                        }
                    }
                }, "yR": {
                    "type": 'linear',
                    "display": true,
                    "position": 'right',
                    "title": {
                        "display": true,
                        "text": 'relatív páratartalom'
                    }, "ticks": {
                        "callback": function(v) {
                            return v.toFixed(1) + ' %';
                        }
                    }
                }
            }
        }
    });
    SensorPoller.init();
}

/**
 * @suppress {undefinedVars}
 */
function updateChart(timeLabels, temperatures, relativeHumidities, airPressures) {
    chart["data"]["datasets"].forEach(function (r) {
        if (r.label === 'Hőmérséklet') {
            r.data = temperatures;
        } else if (r.label === 'Relatív páratartalom') {
            r.data = relativeHumidities;
        } else if (r.label === 'Légnyomás') {
            r.data = airPressures;
        }
    });
    chart.data.labels = timeLabels;
    chart.update();
}

exports = {init, updateChart};
