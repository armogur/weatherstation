Import("env")

try:
    import esptool
except ImportError:
    print("Installing ESP tool")
    env.Execute("$PYTHONEXE -m pip install esptool")

try:
    import rcssmin
except ImportError:
    print("Installing rcssmin")
    env.Execute("$PYTHONEXE -m pip install rcssmin")

try:
    import git
except ImportError:
    print("installing git extension")
    env.Execute("$PYTHONEXE -m pip install gitpython")
